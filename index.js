const express = require('express');
const cors = require('cors');
const fs = require('fs');
const nocache = require('nocache');

const domainName = 'eggyo.dev'
const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/privkey.pem`, 'utf8');
const certificate = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/cert.pem`, 'utf8');
const ca = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/chain.pem`, 'utf8');
const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};

const app = express();
app.use(nocache());
app.use(cors())
app.use(express.static(__dirname + '/build'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/build/index.html'));
});

const port = 443;
const httpsServer = require('https').createServer(credentials, app);
httpsServer.listen(port, function () {
    console.log('web running on port ' + port + '.');
});
