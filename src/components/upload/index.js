import React, { Component } from "react";
import { InboxOutlined } from "@ant-design/icons";
import { Upload, message, Modal } from "antd";
import ImgCrop from 'antd-img-crop';
import globalConfig from "../../config";
const { Dragger } = Upload;

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      previewVisible: false,
      previewImage: "",
    };
  }
  handleCancel = () => this.setState({ previewVisible: false });
  handlePreview = async file => {
    console.log('handlePreviewfile', file);
    if (file.url) {
      this.setState({ previewImage: file.url, previewVisible: true })
    } else {
      getBase64(file.originFileObj, imageUrl => {
        this.setState({ previewImage: imageUrl, previewVisible: true })
      });
    }
  };
  render() {
    const { noCrop } = this.props;
    return (
      <>
        {
          noCrop ? (
            <Dragger
              {...this.props}
              listType="picture"
              maxCount={1}
              action={`${globalConfig.url}:1337`}
              beforeUpload={beforeupload}
              onPreview={this.handlePreview}
            >
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">
                คลิ๊ก หรือ ลากไฟล์มาใส่ เพื่ออัพโหลด
              </p>
            </Dragger>) : (<ImgCrop rotate>
              <Dragger
                {...this.props}
                listType="picture"
                maxCount={1}
                action={`${globalConfig.url}:1337`}
                beforeUpload={beforeupload}
                onPreview={this.handlePreview}
              >
                <p className="ant-upload-drag-icon">
                  <InboxOutlined />
                </p>
                <p className="ant-upload-text">
                  คลิ๊ก หรือ ลากไฟล์มาใส่ เพื่ออัพโหลด
                </p>
              </Dragger>
            </ImgCrop>)
        }

        <Modal
          visible={this.state.previewVisible}
          footer={null}
          onCancel={this.handleCancel}
          width={800}
          style={{ top: 20 }}
        >
          <img
            alt="รูปถ่าย"
            style={{ width: "100%" }}
            src={this.state.previewImage}
          />
        </Modal>
      </>
    );
  }
}

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

export const getUploadValue = (e) => {
  console.log("Upload event:", e);
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

const beforeupload = (file) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt3M = file.size / 1024 / 1024 < 3;
  if (!isLt3M) {
    message.error("Image must smaller than 3MB!");
  }
  return isJpgOrPng && isLt3M || Upload.LIST_IGNORE;
};
