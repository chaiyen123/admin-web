import React, { Component } from "react";
import { InboxOutlined } from "@ant-design/icons";
import { Upload, message, Modal } from "antd";
import globalConfig from "../../config";
const { Dragger } = Upload;

export default class extends Component {
  
  render() {
    return (
      <>
        <Dragger
          {...this.props}
          listType="file"
          maxCount={1}
          action={`${globalConfig.url}:1337`}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
          Click or drag the file here to upload.
          </p>
        </Dragger>
      </>
    );
  }
}

export const getUploadFileValue = (e) => {
  console.log("Upload event:", e);
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

