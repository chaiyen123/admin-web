import React, { Component } from 'react';
import { Select, Row, Col } from 'antd'
import moment from 'moment'
const { Option } = Select;
const monthList = [
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฎาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม'
]

export default class App extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = { day: props.value ? parseInt(props.value.format('DD')) : 1, month: props.value ? parseInt(props.value.format('MM')) : 1, year: props.value ? moment(props.value).year() + 543 : moment().year() + 543 - (props.minAge || 0), }
    }

    updateValues() {
        const { day, month, year } = this.state
        const date = moment(`${year - 543}-${month}-${day}`, 'YYYY-MM-DD').startOf('day')
        this.props.onChange(date)
    }
    render() {
        const { day, month, year } = this.state
        const { minAge, maxAge } = this.props
        const count = [...monthList][month - 1].slice(-2) === 'ยน' ? 30 : [...monthList][month - 1].slice(-2) === 'คม' ? 31 : 29
        const yearOption = !minAge ? year : moment().year() + 543 - minAge
        return (
            <Row gutter={8}>
                <Col xs={8}>
                    <Select showSearch style={{ width: '100%' }} defaultValue={day} onChange={(day) => {
                        this.setState({ day }, () => {
                            this.updateValues()
                        })
                    }}>
                        {
                            [...Array(count)].map((o, i) => <Option value={i + 1} key={i + 1}>{i + 1}</Option>)
                        }
                    </Select>
                </Col>
                <Col xs={8}>
                    <Select style={{ width: '100%' }} defaultValue={month} onChange={(month) => {
                        this.setState({ month }, () => {
                            this.updateValues()
                        })
                    }}>
                        {
                            monthList.map((o, i) => <Option value={i + 1} key={i + 1}>{o}</Option>)
                        }
                    </Select>
                </Col>
                <Col xs={8}>
                    <Select showSearch style={{ width: '100%' }} defaultValue={year} onChange={(year) => {
                        this.setState({ year }, () => {
                            this.updateValues()
                        })
                    }}>
                        {
                            [...Array(minAge && maxAge ? (maxAge - minAge) + 1 : 100)].map((o, i) => <Option value={yearOption + (i * -1)} key={yearOption + (i * -1)}>{yearOption + (i * -1)}</Option>)
                        }
                    </Select>
                </Col>
            </Row>
        );
    }
};

