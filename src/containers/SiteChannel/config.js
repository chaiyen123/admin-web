import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'
const config = {
    className: "SiteChannel",
    defaultQueryKey: "name",
    listTitle: "Site Channel List",
    filterOptions: [
        {
            value: "name",
            label: "Name"
        }
    ],
    columns: [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, 
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config