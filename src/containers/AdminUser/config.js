import moment from 'moment';
const config = {
    className: "User",
    defaultQueryKey: "name",
    createPathUrl: "create-user",
    editPathUrl: "edit-user",
    listTitle: "Admin List",
    filterOptions: [
        {
            value: "name",
            label: "Name"
        },
    ],
    columns: [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
            sorter: (a, b) => {
                if (a.username < b.username)
                    return -1;
                if (a.username > b.username)
                    return 1;
                return 0;
            }
        }, {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => {
                if (a.name < b.name)
                    return -1;
                if (a.name > b.name)
                    return 1;
                return 0;
            }
        }, {
            title: 'Last Login',
            dataIndex: 'lastLogin',
            key: 'lastLogin',
            render: (data) => (<div>{data && moment(data.iso).format('YYYY-MM-DD HH:mm:ss')}</div>)
        }, {
            title: 'Admin Role',
            dataIndex: 'roleName',
            key: 'roleName',
        },
    ]
}

export default config