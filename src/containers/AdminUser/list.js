import React, { Component } from 'react';
import { getAdmins, destroyObjectWithId, adminLogger, getCurrentUser, getRoles, getCurrentUserPermissions } from "../../helpers/parseHelper";
import ContentHolder from '../../components/utility/contentHolder';
import Popconfirms from '../../components/feedback/popconfirm';
import {
    ActionBtn,
    Label,
    TitleWrapper,
    ActionWrapper,
    ComponentTitle,
    TableWrapper,
    ButtonHolders
} from './style';
import { Input, Select, Modal, Button, Switch } from 'antd';
import { withRouter } from "react-router-dom";
import { notification } from "../../components";
import config from "./config.js";
import CreateComponent from "./create"
import EditComponent from "./edit"
const { Search } = Input;
const { Option } = Select;

const className = config.className
const columns = config.columns

class ListTableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataOnTable: [],
            isLoading: true,
            dataLimit: 9999,
            dataSkip: 0,
            isQuery: false,
            searchValue: "",
            selectedRowKeys: [],
            currentPage: 1,
            currentUser: null, pageSize: 20,
            queryKey: config.defaultQueryKey,
            isShowEditModal: false,
            isShowCreateModal: false,
            selectedId: null,
            permissionItems: []
        };
    }

    componentDidMount = async () => {
        const currentUser = await getCurrentUser()
        const permissionItems = await getCurrentUserPermissions(currentUser)
        this.setState({ currentUser, permissionItems }, () => {
            this.simpleLoadData()
        })

    }

    simpleLoadData = async () => {
        const respData = await getAdmins();
        //console.log('respData',respData);
        this.setState({ dataOnTable: respData, isLoading: false });
    }

    handleRecord = async (actionName, obj) => {
        if (actionName == 'delete') {
            const res = await destroyObjectWithId(className, obj.objectId)
            notification(res.type, res.msg)
            await adminLogger('AdminUser', 'Delete', { objectId: obj.objectId })
            this.simpleLoadData()
        } else if (actionName == 'edit') {
            this.setState({ selectedId: obj.objectId }, () => {
                console.log('selectedId', this.state.selectedId);
                this.setState({ isShowEditModal: true })
            });
        }
    };
    handleFilterByChange = (value) => {
        //console.log(`handleFilterByChange : ${value}`)
        this.setState({ queryKey: value })
    }
    handleSearchChange = (event) => {
        this.setState({ searchValue: event.target.value })
    }


    onSelectChange = (selectedRowKeys, selectedRows) => {
        //console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        this.setState({ selectedRowKeys });
    };

    renderCreateModal = () => {
        return (
            <Modal title={`Create ${className}`} visible={this.state.isShowCreateModal} style={{ top: 20 }} footer={null} onCancel={() => { this.setState({ isShowCreateModal: false }) }}>
                <CreateComponent onCreateSuccess={this.onCreateSuccess} />
            </Modal>
        )
    }
    renderEditModal = () => {
        const { selectedId, isShowEditModal } = this.state
        return (
            <Modal title={`Edit ${className}`} visible={isShowEditModal} style={{ top: 20 }} footer={null} onCancel={() => { this.setState({ isShowEditModal: false }) }}>
                <EditComponent onCreateSuccess={this.onCreateSuccess} objectId={selectedId} />
            </Modal>
        )
    }
    onCreateSuccess = () => {
        this.setState({ isShowCreateModal: false });
        this.setState({ isShowEditModal: false });
        this.simpleLoadData();
    }
    render() {
        const { dataOnTable, queryKey, searchValue, currentUser, permissionItems } = this.state;
        let filtered = searchValue === "" ? dataOnTable : dataOnTable.filter(item => item[queryKey].indexOf(searchValue) >= 0)
        const { parentId } = this.props
        if (!parentId) {
            filtered = filtered.filter(item => !item.parentId)
        }
        columns[4] = {
            title: 'Actions',
            width: '60px',
            key: 'action',
            render: (text, row) => {
                return (
                    permissionItems.includes('ADMIN_EDIT') ?
                        <ActionWrapper>
                            <a
                                onClick={this
                                    .handleRecord
                                    .bind(this, 'edit', row)}
                                href="#">
                                <i className="ion-android-create" />
                            </a>

                            <Popconfirms
                                title="Are you sure to delete this record?"
                                okText="Yes"
                                cancelText="No"
                                placement="topRight"
                                onConfirm={this
                                    .handleRecord
                                    .bind(this, 'delete', row)}>
                                <a className="deleteBtn" href="">
                                    <i className="ion-android-delete" />
                                </a>
                            </Popconfirms>
                        </ActionWrapper>
                        : null
                );
            }
        }
        return (
                <ContentHolder>
                    <TitleWrapper>
                        <ComponentTitle>{config.listTitle}</ComponentTitle>
                        {this.renderCreateModal()}
                        {this.renderEditModal()}
                        <ButtonHolders>
                            <Label
                                style={{
                                    marginRight: 15
                                }}>Filter by</Label>
                            <Select
                                defaultValue={config.defaultQueryKey}
                                style={{
                                    width: 120,
                                    marginRight: 15
                                }}
                                onChange={this
                                    .handleFilterByChange
                                    .bind(this)}>
                                {config
                                    .filterOptions
                                    .map(obj => {
                                        return (
                                            <Option value={obj.value}>{obj.label}</Option>
                                        )
                                    })}
                            </Select>
                            <Search
                                allowClear
                                placeholder="input search text"
                                value={this.state.searchValue}
                                onChange={this
                                    .handleSearchChange
                                    .bind(this)}
                                style={{
                                    width: 200,
                                    marginRight: 30
                                }} />
                            <ActionBtn
                                type="default"
                                onClick={() => {
                                    this.setState({
                                        isLoading: true,
                                        dataSkip: 0,
                                        currentPage: 1
                                    }, () => {
                                        this.simpleLoadData()
                                    })
                                }}>
                                Refresh
                            </ActionBtn>
                            {
                                permissionItems.includes('ADMIN_CREATE') ?
                                    <ActionBtn
                                        type="primary"
                                        onClick={() => {
                                            this.setState({ isShowCreateModal: true });
                                        }}>

                                        Create New
                                    </ActionBtn>
                                    : null
                            }

                        </ButtonHolders>
                    </TitleWrapper>
                    <TableWrapper
                        rowKey="objectId"
                        columns={columns}
                        loading={this.state.isLoading}
                        dataSource={filtered}
                        onChange={(pagination) => {
                            console.log('pagination', pagination);
                            const { pageSize } = pagination
                            this.setState({ pageSize })
                        }}
                        pagination={{
                            pageSize: this.state.pageSize,
                            hideOnSinglePage: true
                        }} />
                </ContentHolder> 
        )

    }
}

export default withRouter(ListTableComponent)
