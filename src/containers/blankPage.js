import React, { Component } from 'react';
import LayoutContentWrapper from '../components/utility/layoutWrapper';
import LayoutContent from '../components/utility/layoutContent';
import tableau from 'tableau-api';  

export default class extends Component {
  componentDidMount() {  
    this.initTableau()  
  }  
  initTableau() {
    const vizUrl =
        "http://122.155.11.86/views/DBX1_012/DB1?embed=y&:display_count=no";
    const vizContainer = this.vizContainer;  

    let viz = new window.tableau.Viz(vizContainer, vizUrl,{
      hideTabs: true})  

  }

  render() {
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <LayoutContent>
          <div ref={(div) => { this.vizContainer = div }}>  </div>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
