import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import Async from '../../helpers/asyncComponent';
import moment from 'moment';
import { editObject, getCurrentUser, getObjectWithId, adminLogger, getRoles, getAllObjects } from "../../helpers/parseHelper";
import qs from 'query-string'
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Radio,
    Form,
    DatePicker,
    Upload,
    Icon,
    message,
    Tabs
} from 'antd';
import config from "./config.js";
import { LoadingOutlined, CloudUploadOutlined } from '@ant-design/icons';
const className = config.className
const { Option } = Select;


class EditComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            data: null,
            parseObject: null,
            objectId: null,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
        };

    }
    componentDidMount = async () => {
        this.loadData(this.props)
        const roles = await getRoles()
        this.setState({ roles })
        const country = await getAllObjects('Country')
        this.setState({ country })
        const area = await getAllObjects('Area')
        this.setState({ area })
    }
    UNSAFE_componentWillReceiveProps = async (nextProps) => {
        this.loadData(nextProps)
    }
    loadData = async (props) => {
        const { objectId } = props
        const data = await getObjectWithId(className, objectId);
        const json = data.toJSON()
        console.log('json', json);

        const description = { description: '' };
        this
            .formRef
            .current
            .setFieldsValue(description)

        this.setState({ data }, () => {
            this
                .formRef
                .current
                .setFieldsValue(json)
        })
    }

    handleSubmit = async () => {
        const { data,  area } = this.state
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)
        if (values) {
            if (values.parentID == null || values.parentID == undefined) {
                values.parentID = undefined
                values.parentname = undefined
            } else {
                values.parentname = area.find(item => item.objectId === values.parentID).name || undefined
            }
            values.objectId = data.id;
            await data.save(values, { useMasterKey: true });
            notification('success', 'Save Done');
            await adminLogger(className, 'EDIT', data.toJSON())
            this.props.onCreateSuccess();
        }
    };

    handleCountryIdChange = (value) => {
        const { country } = this.state
        country.map(item => {

            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })

                const data = { countryName: item.name };

                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };

    handleParentIDChange = (value) => {
        const { country, area } = this.state
        if (value==='-'){
            const data = { parentname: '-' };
            this
                .formRef
                .current
                .setFieldsValue(data)
        }

        area.map(item => {
            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })
                const data = { parentname: item.name };
                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            } 
        }
        )
    };


    render() {
        const { country, area, roles } = this.state
        return (
            <Form {...formItemLayout} ref={this.formRef} >
                 <Form.Item
                    name="parentID"
                    label="Parent Area">
                    <Select>
                        <Option value={undefined} key={undefined}>{'No Parent Type'}</Option>
                        {area.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="countryId"
                    label="Country"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Select>
                        {country.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="countryName"
                    label="Name"
                    style={{ display: 'none' }}
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Update
                    </ActionBtn>
                </Form.Item>
            </Form>

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(EditComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};