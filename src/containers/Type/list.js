import React, { Component } from 'react';
import { queryObjectBySomeKey, getAllObjects, destroyObjectWithId, adminLogger, getCurrentUser, getRoles, getCurrentUserPermissions } from "../../helpers/parseHelper";
import ContentHolder from '../../components/utility/contentHolder';
import Popconfirms from '../../components/feedback/popconfirm';
import {
    ActionBtn,
    Label,
    TitleWrapper,
    ActionWrapper,
    ComponentTitle,
    TableWrapper,
    ButtonHolders
} from './style';
import { Input, Select, Modal, Button, Switch } from 'antd';
import { Badge, Dropdown, Menu, Space, Table, Tag, Typography } from 'antd';
import { withRouter } from "react-router-dom";
import { notification } from "../../components";
import config from "./config.js";
import CreateComponent from "./create"
import EditComponent from "./edit"
import { version } from 'nprogress';
const { Search } = Input;
const { Option } = Select;

const className = config.className
const columns = config.columns
const { Title } = Typography;

class ListTableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataOnTable: [],
            isLoading: true,
            dataLimit: 9999,
            dataSkip: 0,
            isQuery: false,
            searchValue: "",
            selectedRowKeys: [],
            currentPage: 1,
            currentUser: null, pageSize: 20,
            queryKey: config.defaultQueryKey,
            isShowEditModal: false,
            isShowCreateModal: false,
            selectedId: null,
            permissionItems: [],
            expandableData: [],
        };
    }

    componentDidMount = async () => {
        const currentUser = await getCurrentUser()
        const permissionItems = await getCurrentUserPermissions(currentUser)
        this.setState({ currentUser, permissionItems }, () => {
            this.simpleLoadData()
        })

    }

    simpleLoadData = async () => {
        
        const rawData = await getAllObjects(className);

        //const rootData = rawData.filter(data => !data.ParentId)
        let temp = []
        rawData.forEach(item => {
            item.children = rawData.filter(data => data.ParentId === item.objectId)
            if(item.children.length === 0){
                delete item.children
            }
            temp.push(item)
        })

        console.log(temp)
        const dataOnTable = temp.filter(item => !item.ParentId)
        this.setState({ dataOnTable, isLoading: false });
        
    }

    LoadExpandableData = async (CurrentRow) => {

        //Load Expandable DataType
        console.log(CurrentRow.objectId);
        const Param = {
            ParenId: CurrentRow.objectId
        }
        const dataOnTable = await queryObjectBySomeKey(className, 'ParentId', CurrentRow.parentId, 9999, 0, false);
        console.log('dataOnTable: ', dataOnTable.results);

        // const filteredRowData = dataOnTable.filter(function (item, id) {
        //     return item.ParentId === CurrentRow.objectId;
        // });
        
        const filteredRowData = dataOnTable.results;
        return filteredRowData;
    };

    handleRecord = async (actionName, obj) => {
        if (actionName == 'delete') {
            const res = await destroyObjectWithId(className, obj.objectId)
            notification(res.type, res.msg)
            await adminLogger('AdminUser', 'Delete', { objectId: obj.objectId })
            this.simpleLoadData()
        } else if (actionName == 'edit') {
            this.setState({ selectedId: obj.objectId }, () => {
                console.log('selectedId', this.state.selectedId);
                this.setState({ isShowEditModal: true })
            });
        }
    };
    handleFilterByChange = (value) => {
        //console.log(`handleFilterByChange : ${value}`)
        this.setState({ queryKey: value })
    }
    handleSearchChange = (event) => {
        this.setState({ searchValue: event.target.value })
    }


    onSelectChange = (selectedRowKeys, selectedRows) => {
        //console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        this.setState({ selectedRowKeys });
    };

    handleExpandableRowRender = (selectedRows) => {
        console.log('selectedRows: ', selectedRows);
    };

    renderCreateModal = () => {
        return (
            <Modal title={`Create ${className}`} visible={this.state.isShowCreateModal} style={{ top: 20 }} footer={null} onCancel={() => { this.setState({ isShowCreateModal: false }) }}>
                <CreateComponent onCreateSuccess={this.onCreateSuccess} />
            </Modal>
        )
    }
    renderEditModal = () => {
        const { selectedId, isShowEditModal } = this.state
        return (
            <Modal title={`Edit ${className}`} visible={isShowEditModal} style={{ top: 20 }} footer={null} onCancel={() => { this.setState({ isShowEditModal: false }) }}>
                <EditComponent onCreateSuccess={this.onCreateSuccess} objectId={selectedId} />
            </Modal>
        )
    }
    onCreateSuccess = () => {
        this.setState({ isShowCreateModal: false });
        this.setState({ isShowEditModal: false });
        this.simpleLoadData();
    }


    menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: 'Action 1',
                },
                {
                    key: '2',
                    label: 'Action 2',
                },
            ]}
        />
    );

    expandedRowRender = (selectedRows, Rows) => {

        //const { expandableData } = this.state;
        //const CurrentRow = JSON.stringify(selectedRows);
        //console.log('หมายเลขคือ: ', selectedRows.objectId)

        //filter by parentId
        const filteredRowData = Rows.filter(function (item, id) {
            //console.log(item);
            return item.ParentId === selectedRows.objectId;
        });

        //console.log('Result is:', filteredRowData);

        const data = [];
        for (let i = 0; i < filteredRowData.length; i++) {
            //console.log('ParentId is:', filteredRowData[i]);
            const item = filteredRowData[i]
            data.push({
                key: item.objectId,
                objectId: item.objectId,
                ParentId: item.ParentId,
                name: item.name,
                description: item.description,
            });
        }

        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                value: "name",
                render: (text, row) => (
                    <Title level={5}>{text}</Title>
                )
            },
            {
                title: 'description',
                dataIndex: 'description',
                key: 'description',
                value: "description",
            },
            {
                title: 'Action',
                dataIndex: 'operation',
                key: 'operation',
                render: (text, row) => (
                    <Space size="middle">
                        <a
                            onClick={this
                                .handleRecord
                                .bind(this, 'edit', row)}
                            href="#">
                            <i className="ion-android-create" />
                        </a>

                        <Popconfirms
                            title="Are you sure to delete this record?"
                            okText="Yes"
                            cancelText="No"
                            placement="topRight"
                            onConfirm={this
                                .handleRecord
                                .bind(this, 'delete', row)}>
                            <a className="deleteBtn" href="">
                                <i className="ion-android-delete" />
                            </a>
                        </Popconfirms>
                    </Space>
                ),
            },
        ];


        return <Table columns={columns} dataSource={data} pagination={false} />;

    };

    render() {
        const { dataOnTable, queryKey, searchValue, currentUser, permissionItems } = this.state;
        let filtered = searchValue === "" ? dataOnTable : dataOnTable.filter(item => item[queryKey].indexOf(searchValue) >= 0)
        const { parentId } = this.props
        if (!parentId) {
            filtered = filtered.filter(item => !item.parentId)
        }  
        columns[4] = {
            title: 'Actions',
            width: '60px',
            key: 'action',
            render: (text, row) => {
                return (
                    permissionItems.includes('ADMIN_EDIT') ?
                        <ActionWrapper>
                            <a
                                onClick={this
                                    .handleRecord
                                    .bind(this, 'edit', row)}
                                href="#">
                                <i className="ion-android-create" />
                            </a>

                            <Popconfirms
                                title="Are you sure to delete this record?"
                                okText="Yes"
                                cancelText="No"
                                placement="topRight"
                                onConfirm={this
                                    .handleRecord
                                    .bind(this, 'delete', row)}>
                                <a className="deleteBtn" href="">
                                    <i className="ion-android-delete" />
                                </a>
                            </Popconfirms>
                        </ActionWrapper>
                        : null
                );
            }
        }


        // const RootTypeList = filtered.filter(function (item, id) {
        //     return item.isRootType === true;
        // })

        // const SubTypeList = filtered.filter(function (item, id) {
        //     return item.isRootType === false;
        // })


        return (



            permissionItems.includes('ADMIN_INFO') ?
                <ContentHolder>
                    <TitleWrapper>
                        <ComponentTitle>{config.listTitle}</ComponentTitle>
                        {this.renderCreateModal()}
                        {this.renderEditModal()}
                        <ButtonHolders>
                            {/* <Label
                                style={{
                                    marginRight: 15
                                }}>Filter by</Label>
                            <Select
                                defaultValue={config.defaultQueryKey}
                                style={{
                                    width: 140,
                                    marginRight: 15
                                }}
                                onChange={this
                                    .handleFilterByChange
                                    .bind(this)}>
                                {config
                                    .filterOptions
                                    .map(obj => {
                                        return (
                                            <Option value={obj.value}>{obj.label}</Option>
                                        )
                                    })}
                            </Select>
                            <Search
                                allowClear
                                placeholder="input search text"
                                value={this.state.searchValue}
                                onChange={this
                                    .handleSearchChange
                                    .bind(this)}
                                style={{
                                    width: 200,
                                    marginRight: 30
                                }} /> */}
                            <ActionBtn
                                type="default"
                                onClick={() => {
                                    this.setState({
                                        isLoading: true,
                                        dataSkip: 0,
                                        currentPage: 1
                                    }, () => {
                                        this.simpleLoadData()
                                    })
                                }}>
                                Refresh
                            </ActionBtn>
                            {
                                permissionItems.includes('ADMIN_CREATE') ?
                                    <ActionBtn
                                        type="primary"
                                        onClick={() => {
                                            this.setState({ isShowCreateModal: true });
                                        }}>

                                        Create New
                                    </ActionBtn>
                                    : null
                            }
                        </ButtonHolders>
                    </TitleWrapper>
                    <TableWrapper
                        rowKey="objectId"
                        columns={columns}
                        loading={this.state.isLoading}
                        dataSource={filtered}
                        expandRowByClick={true}
                        // expandable={{
                        //     expandedRowRender: record => this.expandedRowRender(record, dataOnTable)
                        // }}
                        onChange={(pagination) => {
                            console.log('pagination', pagination);
                            const { pageSize } = pagination
                            this.setState({ pageSize })
                        }}
                        pagination={{
                            pageSize: this.state.pageSize,
                            hideOnSinglePage: true
                        }} />
                </ContentHolder> : null
        )

    }
}

export default withRouter(ListTableComponent)
