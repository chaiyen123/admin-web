import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import { createObject, adminLogger, getCurrentUser, getRoles, getAllObjects } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Form,
} from 'antd';
import config from "./config.js";


const className = config.className
const { Option } = Select;

class CreateComponent extends Component {
    formRef = React.createRef();
    countryNameRef = React.createRef();
    //valueRef = React.useRef();

    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
            types: [],
            TypeName: '',
        };
    }
    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        this.setState({ adminLevel })
        const roles = await getRoles()
        this.setState({ roles })
        const country = await getAllObjects('Country')
        this.setState({ country })
        const area = await getAllObjects('Area')
        this.setState({ area })
        const types = await getAllObjects('DataType')
        this.setState({ types })
    }

    handleSubmit = async e => {
        //console.log('Form Values', values)
        const promise = this.formRef.current.validateFields();
        const { types } = this.state
        const values = await Promise.resolve(promise)
        console.log('values', values)
        if (values) {
            values.ParentName = types.find(item => item.objectId === values.ParentId)?.name || 'undefined'
            const res = await createObject(className, values)
            notification(res.type, res.msg)
            if (res.type == 'success') {
                await adminLogger(className, 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
            }

        }
    };
    render() {
        const { country, area, areaName, types, roles } = this.state

        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}
                onFinishFailed={(errorInfo) => {
                    //console.log('Failed:', errorInfo);
                    this.setState({ loading: false });
                }}
            >
                <Form.Item
                    name="ParentId"
                    label="Parent Type"
                >
                    <Select>
                        <Option value={undefined} key={undefined}>{'No Parent Type'}</Option>
                        {types.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="name"
                    label="Type Name"
                    //style={{ display: 'none' }}
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Save
                    </ActionBtn>
                </Form.Item>
            </Form>
        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};