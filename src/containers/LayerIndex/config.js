import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'
const config = {
    className: "LayerIndex",
    defaultQueryKey: "name",
    listTitle: "Layer Index List",
    filterOptions: [
        {
            value: "name",
            label: "Name"
        },{
            value: "index",
            label: "Index"
        },
    ],
    columns: [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Index',
            dataIndex: 'index',
            key: 'index',
            sorter: (a, b) => sorter(a, b, 'index'),
        }, 
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config