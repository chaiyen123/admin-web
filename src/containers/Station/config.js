import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'

const config = {
    className: "Station",
    defaultQueryKey: "name",
    listTitle: "Station List",
    filterOptions: [
        {
            value: "name",
            key: "name",
            label: "Station Name"
        },{
            value: "countryName",
            key: "countryName",
            label: "Country Name"
        },
    ],
    columns: [
        {
            title: 'Station Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Country Name',
            dataIndex: 'countryName',
            key: 'countryName',
        }, {
            title: 'Area',
            dataIndex: 'areaName',
            key: 'areaName',
        }, 
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config