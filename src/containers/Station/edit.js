import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import Async from '../../helpers/asyncComponent';
import moment from 'moment';
import { editObject, getCurrentUser, getObjectWithId, adminLogger, getRoles, getAllObjects } from "../../helpers/parseHelper";
import qs from 'query-string'
import { ActionBtn } from './style';
import {
    Input,
    InputNumber,
    Select,
    Radio,
    Form,
    DatePicker,
    Upload,
    Icon,
    message,
    Tabs,
    Button,
    Space,
    TreeSelect,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined, DownOutlined  } from '@ant-design/icons';
import config from "./config.js";
import { LoadingOutlined, CloudUploadOutlined } from '@ant-design/icons';
const className = config.className
const { Option } = Select;


class EditComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            data: null,
            parseObject: null,
            objectId: null,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
            areaData: [],
            areaName: '',
            types: [],
            typeData: [],
            parentAreaInfo: [],
            parentTypeInfo: [],
        };

    }
    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        const userData = user.toJSON()
        const data = { adminID: userData.objectId }
        this
            .formRef
            .current
            .setFieldsValue(data)
        this.loadData(this.props)
        const roles = await getRoles()
        this.setState({ roles })
        const country = await getAllObjects('Country')
        this.setState({ country })
        const area = await getAllObjects('Area')
        this.setState({ areaData: area, area })
        

        //map Area data to TreeSelect Format
        let rawData = area.map(data => {
            return {
                name: data.name,
                title: data.name,
                value: data.objectId,
                parentID: data.parentID,
                objectId: data.objectId,
            }
        });

        let rootArea = rawData.filter(data => data.parentID === undefined);
        rawData.map(item => {
            const CurrentItem = rawData.filter(data => data.parentID === item.objectId)
            item.children = CurrentItem
            //temp.push(item);
            //console.log('CurrentItem:', CurrentItem);
        })
        //console.log('New Data:', rootArea);
        this.setState({ area: rootArea });


       
    }
    UNSAFE_componentWillReceiveProps = async (nextProps) => {
        this.loadData(nextProps)
    }
    loadData = async (props) => {
        const { objectId } = props
        const data = await getObjectWithId(className, objectId);
        const json = data.toJSON()
        //console.log('json', json);

        this.setState({ parentAreaInfo: json.areaData });

        //Clear MetaData In FormRef
        const MetaData = { MetaData: [] };
        this
            .formRef
            .current
            .setFieldsValue(MetaData)


        this.setState({ data }, () => {
            this
                .formRef
                .current
                .setFieldsValue(json)
        })
    }

    handleSubmit = async () => {
        const { data, areaData, country, typeData, parentAreaInfo } = this.state
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)

        //getCurrentUser and set current user value to formRef
        const user = await getCurrentUser()

        if (values) {

            values.objectId = data.id;
            values.admin = user; //store current user to admin field pointer
            values.areaName = areaData.find(item => item.objectId === values.areaId).name || undefined
            values.countryName = country.find(item => item.objectId === values.countryId).name || undefined
            values.areaData = parentAreaInfo
            await data.save(values, { useMasterKey: true });
            notification('success', 'Save Done');
            await adminLogger(className, 'EDIT', data.toJSON())
            this.props.onCreateSuccess();
        }
    };

    handleCountryIdChange = (value) => {
        const { country } = this.state
        country.map(item => {

            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })

                const data = { countryName: item.name };

                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };

    handleParentIDChange = (value) => {
        const { country, area } = this.state
        if (value === '-') {
            const data = { parentname: '-' };
            this
                .formRef
                .current
                .setFieldsValue(data)
        }

        area.map(item => {
            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })
                const data = { parentname: item.name };
                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };


    
    //Function Get All Parents Area
    getAllParents(parentID) {
        const { areaData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = areaData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                const Info = JSON.stringify(parentInfo);
                //console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.parentID;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

        
    onAreaSelect = (selectedKeys, info) => {
        //console.log('selectedKeys', info.objectId);
        const parentID = info.parentID;
        const parentAreaInfo = this.getAllParents(parentID);
        this.setState({ parentAreaInfo });
        const data = { areaName: info.name, areaId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    

    render() {
        const { country, area, areaName, types , roles } = this.state
        return (
            <Form {...formItemLayout} ref={this.formRef} >
                <Form.Item
                    name="name"
                    label="Station Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="stationCode"
                    label="Station Code"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="countryId"
                    label="Country"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Select>
                        <Option value={'-'} key={'-'}>{'ไม่กำหนด'}</Option>
                        {country.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="areaId"
                    label="Area"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={area}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                        onSelect={this.onAreaSelect}
                    />
                </Form.Item>
                
                <Form.Item
                    name="lat"
                    label="Latitude"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="lng"
                    label="Longitude"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="adminID"
                    label="Admin ID"
                    style={{ display: 'none' }}
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>

                <Form.List
                    name="MetaData"
                    label="MetaData"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <Space
                                    key={key}
                                    style={{
                                        display: 'flex',
                                        marginBottom: 8,
                                        marginLeft: 50,
                                    }}
                                    align="baseline"
                                >
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'Key']}
                                        label="Key"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Missing Key name',
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Key" />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'Value']}
                                        label="Value"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Missing Value',
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Value" />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                </Space>
                            ))}
                            <Form.Item
                                style={
                                    {
                                        marginLeft: 80,
                                    }
                                }
                            >
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add MetaData field
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>

                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Update
                    </ActionBtn>
                </Form.Item>
            </Form>

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(EditComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};