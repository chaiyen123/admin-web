import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import Async from '../../helpers/asyncComponent';
import moment from 'moment';
import { uploadParseVideo,uploadParseFile, getCurrentUser, getObjectWithId, adminLogger, getRoles, getAllObjects } from "../../helpers/parseHelper";
import qs from 'query-string'
import { ActionBtn } from './style';
import {
    Input,
    InputNumber,
    Select,
    Radio,
    Form,
    DatePicker,
    Icon,
    message,
    Tabs,
    Button,
    Space,
    TreeSelect,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined, DownOutlined } from '@ant-design/icons';
import config from "./config.js";
import { LoadingOutlined, CloudUploadOutlined } from '@ant-design/icons';
import Upload, { getUploadFileValue } from '../../components/uploadFile'
import DropdownDate from '../../components/dropdownDate'

const className = config.className
const { Option } = Select;


class EditComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            data: null,
            parseObject: null,
            objectId: null,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
            areaData: [],
            areaName: '',
            parentInfo: [],
            types: [],
            typeData: [],
            parentAreaInfo: [],
            parentTypeInfo: [],
            layerIndex: [],
            layerData: null,
        };

    }
    componentDidMount = async () => {
        this.setState({ loading: true });
        const parse = await qs.parse(window.location.search)
        const { objectId } = parse
        console.log('componentDidMount objectId',objectId);
        const layerData = await getObjectWithId('LayerList', objectId);
        const json = layerData.toJSON();
        console.log('LayerList Data:', json);
        this.setState({ layerData: json });
        const user = await getCurrentUser()
        const userData = user.toJSON()
        const data = { adminID: userData.objectId }
        this
            .formRef
            .current
            .setFieldsValue({...json,...data})
        this.loadData(this.props)
        const roles = await getRoles()
        this.setState({ roles })
        this.setState({ loading: false });
    }
    

    UNSAFE_componentWillReceiveProps = async (nextProps) => {
        console.log('nextProps',nextProps);
        this.loadData(nextProps)
    }

    loadData = async (props) => {
        const { objectId } = props
        console.log('loadData objectId',objectId);
        const data = await getObjectWithId(className, objectId);
        const json = data.toJSON()
        //console.log('json', json);
        json.fileDate = moment(json.fileDate.iso);
        this.setState({ data }, () => {
            this
                .formRef
                .current
                .setFieldsValue(json)
        })
    }

    handleSubmit = async () => {
        //console.log('Form Values', values)
        const promise = this.formRef.current.validateFields();
        const { data, layerData } = this.state
        this.setState({ loading: true });
        const values = await Promise.resolve(promise)
        const user = await getCurrentUser()
        if (values) {
            console.log('values',values);
            values.objectId = data.id;
            values.layerId = data.layerId;
            values.admin = user;
            const { countryId, countryName, areaId, areaName, typeId, typeName, layerIndex } = layerData

            if (values.fileDate) {
                values.fileDate = values.fileDate.toDate();
            }

            if (values.fileDate) {
                if (values.fileDate instanceof moment) {
                    values.fileDate = values.fileDate.toDate()
                }
            }

            if (!values.fileData[0].url) {
                if (values.fileData[0]) {
                    const file = await uploadParseFile(values.fileData[0]);
                    values.fileData[0].url = file.url;
                    values.fileName = values.fileData[0]['name'];
                    values.fileUrl = file.url;
                }
            } else {
                console.log('not upload fileData!');
            }

            if (!values.fileLegend[0].url) {
                if (values.fileLegend[0]) {
                    const file = await uploadParseFile(values.fileLegend[0]);
                    values.fileLegend[0].url = file.url;
                    values.legendFileName = values.fileLegend[0]['name'];
                    values.legendUrl = file.url;
                }
            } else {
                console.log('not upload fileData!');
            }

            if (!values.fileClip[0]?.url) {
                console.log('thisssss');
                if (values.fileClip[0]) {
                    const file = await uploadParseVideo(values.fileClip[0],true);
                    values.fileClip[0].url = file.url;
                    values.clipFileName = values.fileClip[0]['name'];
                    values.clipUrl = file.url;
                }
            } else {
                console.log('not upload fileData!');
            }

            await data.save({ ...values, countryId, countryName, areaId, areaName, typeId, typeName, layerIndex }, { useMasterKey: true });
            notification('success', 'Save Done');
            await adminLogger(className, 'EDIT', data.toJSON())
            this.formRef.current.resetFields();
            this.props.onCreateSuccess();
            this.setState({ loading: false });
        }
    };

    //Function Get All Parents Area
    getAllParents(parentID) {
        const { areaData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = areaData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                const Info = JSON.stringify(parentInfo);
                //console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.parentID;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    //Function Get All Parents Type
    getAllParentsType(parentID) {
        const { typeData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = typeData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.ParentId;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    onAreaSelect = (selectedKeys, info) => {
        console.log('selectedKeys', selectedKeys);
        const parentID = info.parentID;
        const parentAreaInfo = this.getAllParents(parentID);
        this.setState({ parentAreaInfo });
    };

    onTypeSelect = (selectedKeys, info) => {
        console.log('selectedKeys', info);
        const parentId = info.parentId;
        const parentTypeInfo = this.getAllParentsType(parentId);

        this.setState({ parentTypeInfo });
        const data = { typeName: info.name, typeId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    render() {
        const { country, area, areaName, types, roles, layerIndex } = this.state
        return (
            <Form {...formItemLayout} ref={this.formRef}
                onFinishFailed={(errorInfo) => {
                    //console.log('Failed:', errorInfo);
                    this.setState({ loading: false });
                }}
            >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>

                <Form.Item
                    name="north"
                    label="North"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="south"
                    label="South"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="east"
                    label="East"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="west"
                    label="West"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="fileDate"
                    label="Date"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <DatePicker />
                </Form.Item>
                <Form.Item name="fileData" label="Layer File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    <Upload />
                </Form.Item>
                <Form.Item name="fileLegend" label="Legend File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    <Upload />
                </Form.Item>
                <Form.Item name="fileClip" label="Clip File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}>
                    <Upload />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        loading={this.state.loading}
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Update
                    </ActionBtn>
                </Form.Item>
            </Form>

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(EditComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};