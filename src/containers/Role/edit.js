import React, { Component } from 'react';
import { connect } from "react-redux";
import { notification } from "../../components";
import { getAllObjects, createObject, getCurrentUser, adminLogger, getObjectWithId } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Radio,
    Form,
    InputNumber,
    Row,
    Col
} from 'antd';
import config from "./config.js";
import axios from 'axios'
import globalConfig from "../../config";
const className = config.className
const { Option } = Select;
const API_URL = globalConfig.parseServerUrl.replace('/parse', '/api/v1');



class EditComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            data: null
        };

    }
    componentDidMount = async () => {
        this.loadData(this.props)
    }
    UNSAFE_componentWillReceiveProps = async (nextProps) => {
        this.loadData(nextProps)
    }

    loadData = async (props) => {
        const { objectId } = props
        const data = await getObjectWithId(className, objectId);
        const json = data.toJSON()
        this.setState({ data }, () => {
            const permissionItems = data ? data.toJSON().permissionItems : []
            const rawData = data ? {
                name: data.toJSON().name,
                ADMIN: permissionItems.filter(item => item.indexOf('ADMIN') === 0),
                ROLE: permissionItems.filter(item => item.indexOf('ROLE') === 0),
                ACTIVITY: permissionItems.filter(item => item.indexOf('ACTIVITY') === 0),
                SETTING: permissionItems.filter(item => item.indexOf('SETTING') === 0),
                STATION: permissionItems.filter(item => item.indexOf('STATION') === 0),
                OVERLAY: permissionItems.filter(item => item.indexOf('OVERLAY') === 0),
                AREA: permissionItems.filter(item => item.indexOf('AREA') === 0),
                COUNTRY: permissionItems.filter(item => item.indexOf('COUNTRY') === 0),
                TYPE: permissionItems.filter(item => item.indexOf('TYPE') === 0),
                LAYERS: permissionItems.filter(item => item.indexOf('LAYERS') === 0),
                SITE: permissionItems.filter(item => item.indexOf('SITE') === 0),
            } : {}

            this
                .formRef
                .current
                .setFieldsValue(rawData)
        })

    }

    handleSubmit = async e => {
        const { data } = this.state
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise);
        if (values) {
            try {
                console.log('valuse', values);
                const saveData = { name: values.name }
                delete values.name
                const list = Object.values(values).filter(item => item !== undefined)
                let permissionItems = []
                list.forEach(item => {
                    permissionItems = [...permissionItems, ...item]
                })
                saveData.permissionItems = permissionItems
                await data.save(saveData, { useMasterKey: true });
                notification('success', 'Save Done');
                await adminLogger('Roles', 'EDIT', values)
                //this.formRef.current.resetFields();
                this.props.onCreateSuccess();
                this.setState({ loading: false })
            } catch (error) {
                notification('error', error.message);
            }
        }
    };


    render() {
        const { data } = this.state;


        return (
            data ?
                <Form
                    {...formItemLayout}
                    ref={this.formRef}>
                    <Form.Item
                        name="name"
                        label="Role Name"
                        rules={[{
                            required: true,
                            message: 'Please fill data!'
                        }
                        ]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="ADMIN" label="Admin" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('ADMIN') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="ROLE" label="Permission" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('ROLE') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="ACTIVITY" label="Activity" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('ACTIVITY') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="SETTING" label="Setting" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('SETTING') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="STATION" label="Station Data" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('STATION') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="OVERLAY" label="Overlay Data" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('OVERLAY') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="AREA" label="Area Data" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('AREA') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="COUNTRY" label="Country Data" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('COUNTRY') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="TYPE" label="Data Type" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('TYPE') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="LAYERS" label="Layers Data" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('LAYERS') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="SITE" label="Site Data" >
                        <Select mode="multiple" placeholder="Please select" >
                            {
                                config.permissionsList.filter(item => item.indexOf('SITE') === 0).map(item => (<Option key={item}>{item}</Option>))
                            }
                        </Select>
                    </Form.Item>

                    <Form.Item {...tailFormItemLayout}>
                        <center>
                            <ActionBtn
                                type="primary"
                                htmlType="submit"
                                onClick={this.handleSubmit}
                                style={{
                                    width: 120
                                }}>
                                Submit
                            </ActionBtn>
                        </center>
                    </Form.Item>
                </Form> : null

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(EditComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 8
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 24,
            offset: 0
        }
    }
};
