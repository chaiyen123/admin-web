import React from 'react';
import { Tag,Statistic } from 'antd';
import moment from 'moment';
const config = {
    className: "Roles",
    defaultQueryKey: "name",
    createPathUrl: "create-role",
    editPathUrl: "edit-role",
    listTitle: "Role Permission",
    permissionsList :[
        "ADMIN_INFO",
        "ADMIN_CREATE",
        "ADMIN_EDIT",
        "ROLE_INFO",
        "ROLE_CREATE",
        "ROLE_EDIT",
        "ACTIVITY",
        "SETTING",
        "STATION_ITEM_INFO",
        "STATION_ITEM_CREATE",
        "STATION_ITEM_EDIT",
        "OVERLAY_ITEM_INFO",
        "OVERLAY_ITEM_CREATE",
        "OVERLAY_ITEM_EDIT",
        "AREA_ITEM_INFO",
        "AREA_ITEM_EDIT",
        "AREA_ITEM_CREATE",
        "COUNTRY_ITEM_INFO",
        "COUNTRY_ITEM_CREATE",
        "COUNTRY_ITEM_EDIT",
        "TYPE_ITEM_INFO",
        "TYPE_ITEM_CREATE",
        "TYPE_ITEM_EDIT",
        "LAYERS_ITEM_INFO",
        "LAYERS_ITEM_CREATE",
        "LAYERS_ITEM_EDIT",
        "LAYER_INDEX_INFO",
        "LAYER_INDEX_CREATE",
        "LAYER_INDEX_EDIT",
        "SITE_CHANNEL_INFO",
        "SITE_CHANNEL_CREATE",
        "SITE_CHANNEL_EDIT",
    ],    
    filterOptions: [
        {
            value: "name",
            label: "Role Name"
        }
    ],
    columns: [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            width:120,
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Permission Items',
            dataIndex: 'permissionItems',
            key: 'permissionItems',
            sorter: (a, b) => sorter(a, b, 'permissionItems'),
            render: (data, obj) => {
                const role = data.map((item,) => {
                    return (
                        <Tag>{item}</Tag>
                    )
                })
                return role
            }
        }, 
    ]
}
const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}

const statusStyle = {color: 'white', fontWeight: 'bold',width:20, height:20,borderRadius:10}

export default config