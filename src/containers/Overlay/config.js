import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'
const config = {
    className: "Overlay",
    defaultQueryKey: "name",
    listTitle: "Overlay List",
    filterOptions: [
        {
            value: "name",
            label: "Overlay Name"
        },{
            value: "countryCode",
            label: "Overlay Code"
        },
    ],
    columns: [
        {
            title: 'Overlay Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Overlay Code',
            dataIndex: 'countryCode',
            key: 'countryCode',
            sorter: (a, b) => sorter(a, b, 'countryCode'),
        }, {
            title: 'Site Count',
            dataIndex: 'site',
            key: 'site',
            render: (data) => <Tag>{data}</Tag>
        }, {
            title: 'province',
            dataIndex: 'province',
            key: 'province',
        }, 
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config