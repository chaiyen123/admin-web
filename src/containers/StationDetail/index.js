import React, { Component } from "react";
import Box from "../../components/utility/box";
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import { Col, Row, Checkbox } from 'antd';
import { getObjectWithId } from "../../helpers/parseHelper";
import StationData from "../StationData";
import qs from 'query-string'
import {
  ActionBtn,
  Label,
  TitleWrapper,
  ActionWrapper,
  ComponentTitle,
  TableWrapper,
  ButtonHolders
} from './style';


export default class BlogComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stationData: null,
    };
}

  componentDidMount = async () => {
    const parse = await qs.parse(window.location.search)
    this.loadData(parse)
  }

  loadData = async (parse) => {
    const { objectId } = parse
    const stationData = await getObjectWithId('Station',objectId);
    const json  = stationData.toJSON();
    this.setState({ stationData: json });
  }

  render() {
    const { stationData } = this.state;
    console.log('StationData', stationData);
    return (
      <LayoutWrapper>
        <TitleWrapper>
         {stationData && (<ComponentTitle>{`สถานี : ${stationData.name}`}</ComponentTitle>)} 
        </TitleWrapper>
        <Box>
          <Row gutter={[3, 3]}>
            <Col span={24}>
              <StationData objectId />
            </Col>
          </Row>
        </Box>
      </LayoutWrapper>
    );
  }
}
