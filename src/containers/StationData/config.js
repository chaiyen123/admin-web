import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'

const config = {
    className: "StationData",
    defaultQueryKey: "name",
    listTitle: "Station Data",
    filterOptions: [
        {
            value: "name",
            key: "name",
            label: "Name"
        }
    ],
    columns: [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Type',
            dataIndex: 'typeName',
            key: 'typeName',
            sorter: (a, b) => sorter(a, b, 'typeName'),
        }, {
            title: 'visualize Format',
            dataIndex: 'visualizeFormat',
            key: 'visualizeFormat',
            sorter: (a, b) => sorter(a, b, 'visualizeFormat'),
        }, 
        {
            title: 'File Name',
            dataIndex: 'fileName',
            key: 'fileName',
            sorter: (a, b) => sorter(a, b, 'fileName'),
            render: (text, record) => {
                return (
                    <a href={record.fileUrl} target="_blank">{text}</a>
                )
            }
        }
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config